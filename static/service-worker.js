importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.1.1/workbox-sw.js');

if (workbox) {
	console.log(`Yay! Workbox is loaded 🎉`);
} else {
	console.log(`Boo! Workbox didn't load 😬`);
}

workbox.routing.registerRoute(/\.css$/, new workbox.strategies.CacheFirst({
	cacheName: 'cache_css'
}));

workbox.routing.registerRoute(/\.js$/, new workbox.strategies.StaleWhileRevalidate({
	cacheName: 'cache_js'
}));

workbox.routing.registerRoute(/\.(?:png|jpg|jpeg|svg|gif)$/, new workbox.strategies.CacheFirst({
	cacheName: 'cache_img',
	plugins: [
		new workbox.expiration.Plugin({
			maxEntries: 20,
			maxAgeSeconds: 7 * 24 * 60 * 60,
		})
	],
}));
