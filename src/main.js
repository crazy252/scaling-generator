import Vue from 'vue';
import App from './App';

if ('serviceWorker' in navigator) {
	window.addEventListener('load', () =>{
		navigator.serviceWorker.register('static/service-worker.js');
	});
}

Vue.config.productionTip = false;

new Vue({
	el: '#app',
	components: {App},
	template: '<App/>'
});
