<template>
	<div id="app" class="d-flex justify-content-center h-100" v-cloak>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h1 class="display-1 mt-3 mb-4">Scaling Generator</h1>
				</div>
				<div class="col-12">
					<form @submit.prevent="add">
						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<label for="selector">Selector</label>
									<input type="text" id="selector" class="form-control" placeholder="h1, span, .my-class, ..." v-model="selector" />
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									<label for="property">Property</label>
									<input type="text" id="property" class="form-control" placeholder="font-size, line-height, ..." v-model="property" />
								</div>
							</div>
							<div class="col-3">
								<div class="form-group">
									<label for="breakpointMin">Minimum breakpoint</label>
									<div class="input-group">
										<input type="number" id="breakpointMin" class="form-control" min="0" max="9999" placeholder="1, 2, 3, ..." v-model="breakpointMin" />
										<div class="input-group-append">
											<label for="breakpointMin" class="input-group-text">px</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-3">
								<div class="form-group">
									<label for="breakpointMax">Maximum breakpoint</label>
									<div class="input-group">
										<input type="number" id="breakpointMax" class="form-control" min="0" max="9999" placeholder="1, 2, 3, ..." v-model="breakpointMax" />
										<div class="input-group-append">
											<label for="breakpointMax" class="input-group-text">px</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-3">
								<div class="form-group">
									<label for="valueMin">Minimum value</label>
									<div class="input-group">
										<input type="number" id="valueMin" class="form-control" min="0" max="9999" placeholder="1, 2, 3, ..." v-model="valueMin" />
										<div class="input-group-append">
											<label for="valueMin" class="input-group-text">px</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-3">
								<div class="form-group">
									<label for="valueMax">Maximum value</label>
									<div class="input-group">
										<input type="number" id="valueMax" class="form-control" min="0" max="9999" placeholder="1, 2, 3, ..." v-model="valueMax" />
										<div class="input-group-append">
											<label for="valueMax" class="input-group-text">px</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-12">
								<div class="row justify-content-center">
									<div class="col-2">
										<button type="reset" class="btn btn-block btn-outline-danger" @click="reset">Reset</button>
									</div>
									<div class="col-2">
										<button type="submit" class="btn btn-block btn-outline-success">Add</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<hr class="mt-4 mb-4" />
				</div>
				<div class="col-12">
					<ul class="nav nav-pills justify-content-center">
						<li class="nav-item">
							<a href="#" class="nav-link" :class="tabActive('list')" @click="tab = 'list'">List</a>
						</li>
						<li class="nav-item">
							<a href="#" class="nav-link" :class="tabActive('code')" @click="tab = 'code'">Code</a>
						</li>
						<li class="nav-item">
							<a href="#" class="nav-link" :class="tabActive('diagram')" @click="tab = 'diagram'">Diagram</a>
						</li>
					</ul>
				</div>
				<div class="col-12 pt-3">
					<List :list="listComponent" v-if="tab === 'list'" v-on:remove="remove" />
					<Code :code="codeComponent" v-if="tab === 'code'" />
					<Diagramm :diagram="diagramComponent" v-if="tab === 'diagram'" />
				</div>
				<div class="col-12">
					<p class="mt-5 mb-0 text-center">Made with <3 by <a href="https://crazy252.de/" target="_blank">crazy252</a></p>
				</div>
			</div>
		</div>
	</div>
</template>

<script>
	import List from "@/components/List";
	import Code from "@/components/Code";
	import Diagramm from "@/components/Diagramm";

	export default {
		name: 'App',
		components: {Diagramm, Code, List},
		data() {
			return {
				items: [],
				selector: '',
				property: '',
				valueMin: 0,
				valueMax: 0,
				breakpointMin: 0,
				breakpointMax: 0,
				tab: 'list'
			}
		},
		created() {
			let loading = document.getElementById('loading');
			if (loading)
				loading.remove();
		},
		computed: {
			listComponent() {
				return this.breakpointSort();
			},
			codeComponent() {
				return this.createCss(this.breakpointSort());
			},
			diagramComponent() {
				let items = this.breakpointSort(), diagram = {x: {min: 9999, max: 0}, y: {min: 9999, max: 0}, lines: {}};

				for (let index in items) {

					let item = items[index];
					let key = item.selector.toLowerCase().replace(' ', '0').replace('-', '_') + '__' + item.property.toLowerCase().replace(' ', '0').replace('-', '_');

					if (diagram.x.min > item.breakpointMin)
						diagram.x.min = item.breakpointMin;

					if (diagram.x.max < item.breakpointMax)
						diagram.x.max = item.breakpointMax;

					if (diagram.y.min > item.valueMin)
						diagram.y.min = item.valueMin;

					if (diagram.y.max < item.valueMax)
						diagram.y.max = item.valueMax;

					if (!Object.keys(diagram.lines).includes(key))
						diagram.lines[key] = {};

					let keyMin = item.breakpointMin.toString().padStart(4, '0') + '__' + item.valueMin.toString().padStart(3, '0');
					if (!Object.keys(diagram.lines[key]).includes(keyMin))
						diagram.lines[key][keyMin] = {x: item.breakpointMin, y: item.valueMin};

					let keyMax = (item.breakpointMax + 1).toString().padStart(4, '0') + '__' + item.valueMax.toString().padStart(3, '0');
					if (!Object.keys(diagram.lines[key]).includes(keyMax))
						diagram.lines[key][keyMax] = {x: (item.breakpointMax + 1), y: item.valueMax};

				}

				return diagram;
			}
		},
		methods: {
			reset() {
				this.valueMin = 0;
				this.valueMax = 0;
				this.breakpointMin = 0;
				this.breakpointMax = 0;
				this.selector = '';
				this.property = '';
			},
			add() {
				if (this.selector.length === 0)
					return;

				if (this.property.length === 0)
					return;

				if (this.breakpointMin.length === 0 || this.breakpointMin < 1)
					return;

				if (this.breakpointMax.length === 0 || this.breakpointMax < 1)
					return;

				if (this.valueMin.length === 0 || this.valueMin < 1)
					return;

				if (this.valueMax.length === 0 || this.valueMax < 1)
					return;

				let valueMin = parseInt(this.valueMin);
				let valueMax = parseInt(this.valueMax);
				let breakpointMin = parseInt(this.breakpointMin);
				let breakpointMax = parseInt(this.breakpointMax);
				let selector = this.selector.trim();
				let property = this.property.trim();

				let key = '';
				key += selector.toLowerCase().replace(' ', '0').replace('-', '_') + '__';
				key += property.toLowerCase().replace(' ', '0').replace('-', '_') + '__';
				key += breakpointMin.toString().padStart(4, '0') + '_' + breakpointMax.toString().padStart(4, '0') + '__';
				key += valueMin.toString().padStart(3, '0') + '_' + valueMax.toString().padStart(3, '0');

				this.items.push({key: key, valueMin: valueMin, valueMax: valueMax, breakpointMin: breakpointMin, breakpointMax: breakpointMax, selector: selector, property: property});
			},
			remove(key) {
				let index = null;

				for (let itemIndex in this.items) {
					let item = this.items[itemIndex];

					if (item.key === key)
						index = itemIndex;
				}

				let removed = this.items.splice(index, 1);
			},
			tabActive(tab) {
				return {'active' : this.tab === tab};
			},
			breakpointSort() {
				return this.items.sort((current, next) => current.key > next.key ? 1 : -1);
			},
			createCss(items) {
				let code = [];

				for (let item of items) {
					code.push('@media (min-width: ' + item.breakpointMin + 'px) and (max-width: ' + item.breakpointMax + 'px) {\n\t' + item.selector + ' {\n\t\t' + item.property + ': calc(' + item.valueMin + 'px + (' + item.valueMax + ' - ' + item.valueMin + ') * (100vw - ' + item.breakpointMin + 'px) / (' + item.breakpointMax + ' - ' + item.breakpointMin + '));\n\t}\n}');
				}

				return code.join('\n');
			}
		}
	};
</script>

<style>
	html, body {
		height: 100%;
	}
	[v-cloak] {
		display: none;
	}
</style>
